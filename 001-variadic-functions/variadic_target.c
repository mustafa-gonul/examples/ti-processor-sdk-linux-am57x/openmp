
#pragma omp declare target
#include <omp.h>
#include <stdarg.h>
#pragma omp end declare target

int add_nums(int count, ...)
{
    int result = 0;
    int i = 0;

    va_list args;
    va_start(args, count);

    for (i = 0; i < count; ++i) {
        result += va_arg(args, int);
    }
    va_end(args);

    return result;
}


