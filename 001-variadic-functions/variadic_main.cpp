#include <cstdio>

extern "C" {
    extern int add_nums(int count, ...);
}

int main(int argc, char *argv[])
{
    int total = add_nums(4, 1, 2, 3, 4);

    printf("total: %d\n", total);

    return 0;
}
