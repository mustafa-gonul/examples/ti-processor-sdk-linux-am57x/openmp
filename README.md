# OpenMP Examples

This repository is created for testing OpenMP and compiler features in addition to the examples in the SDK.

## Examples

### Variadic Arguments (001-variadic-functions)

This example is created to test compiler if it can compile variadic arguments. In newer SDKs there are some problems to compile this feature.

Problematic SDKs:

- ti-processor-sdk-linux-rt-am57xx-evm-06.00.00.07
- ti-processor-sdk-linux-rt-am57xx-evm-06.01.00.08

### Variadic Macros (002-variadic-macro)

The example is created to test if the variadic arguments for the macros.

### Print Debug

The example is copied from the SDK. The example aims to test if OpenMP features can be used and to check if offloading is working.

**References:**

- [cppreference.com - C++](https://en.cppreference.com/w/cpp/language/variadic_arguments)
- [cppreference.com - C](https://en.cppreference.com/w/c/language/variadic)
