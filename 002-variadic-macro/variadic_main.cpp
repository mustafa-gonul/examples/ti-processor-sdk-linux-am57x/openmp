#include <cstdio>

extern "C" {
    void print_variadic_macro();
}

int main(int argc, char *argv[])
{
    print_variadic_macro();

    return 0;
}
