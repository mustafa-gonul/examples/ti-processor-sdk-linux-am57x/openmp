
#pragma omp declare target
#include <omp.h>
extern int printf(const char *_format, ...);
#pragma omp end declare target

#define VARIADIC_STR(...) #__VA_ARGS__

void print_variadic_macro()
{
  #pragma omp target
  {
    printf(VARIADIC_STR(1, 2, 3, 4, 5));
  }
}


